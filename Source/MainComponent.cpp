/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    deviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    deviceManager.addAudioCallback (this);
    
    
    StringArray sa (MidiOutput::getDevices());
    for (String& s : sa)
    {
        DBG(s);
    }
    
    //initialise MIDI
    deviceManager.setMidiInputEnabled ("USB Axiom 49 Port 1", true);
    deviceManager.setDefaultMidiOutput ("SimpleSynth virtual input");
    deviceManager.addMidiInputCallback(String::empty, this);
    
    ampLabel.setText("Amplitude", dontSendNotification);
    addAndMakeVisible(ampLabel);
    
    ampSlider.setSliderStyle(Slider::Rotary);
    ampSlider.setRange(0, 1);
    addAndMakeVisible(ampSlider);
    ampSlider.addListener(this);
}

MainComponent::~MainComponent()
{
    deviceManager.removeAudioCallback (this);
    deviceManager.removeMidiInputCallback (String::empty, this);
}

void MainComponent::resized()
{
    ampSlider.setBounds(10, 10, 150, 40);
    ampLabel.setBounds(90, 40, getWidth() - 20, 40);
    
}

void MainComponent::audioDeviceIOCallback(const float** inputChannelData,
                                          int numInputChannels,
                                          float** outputChannelData,
                                          int numOutputChannels,
                                          int numSamples)
{
    const float *inL = inputChannelData[0]; const float *inR = inputChannelData[1]; float *outL = outputChannelData[0]; float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        *outL = (*inL * amplitude);
        *outR = (*inR * amplitude);
        inL++;
        outL++;
        outR++;
    }
}

void MainComponent::audioDeviceAboutToStart(AudioIODevice* device)
{
    DBG("Audio Device About to Start");
}

void MainComponent::audioDeviceStopped()
{
    DBG("Audio Device Stopped");
}

void MainComponent::sliderValueChanged(Slider* slider)
{
   
    DBG("Slider value changed");
}

void MainComponent::handleIncomingMidiMessage(MidiInput* source, const MidiMessage& message_)
{
    if (message_.isController())
    {
        ampSlider.getValueObject().setValue(message_.getControllerValue()/127.0);
    }

    deviceManager.getDefaultMidiOutput() -> sendMessageNow(message_);
    
}
